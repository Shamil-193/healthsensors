export interface SelectedSchedule {
   [key: string]: {AM: string[], PM: string[]} 
}

export interface ScheduleCard {
   runTime: number;
}