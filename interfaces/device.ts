export interface Device {
    image: string;
    title: string;
    subtitle: string;
}