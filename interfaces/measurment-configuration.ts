export interface Measurment {
    title: string;
    image: string;
    toggles: Toggles[];
    options?: Option[];
}

export interface Option {
    name: string;
    value: string;
}

export interface Toggles {
    name: string;
    status: boolean;
}

