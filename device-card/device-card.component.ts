import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Device } from '../interfaces/device';
import { DataSourceService } from '../services/data-source.service';

@Component({
  selector: 'app-device-card',
  templateUrl: './device-card.component.html',
  styleUrls: ['./device-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeviceCardComponent implements OnInit {

  deviceCard$: Observable<Device>;

  constructor(private dataSourceService: DataSourceService) { }

  ngOnInit(): void {
    this.deviceCard$ = this.dataSourceService.createDeviceCardStream();
  }


}
