import { NgModule } from '@angular/core';
import { MatButtonModule, MatCardModule, MatDialogModule, MatProgressSpinnerModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DeviceCardComponent } from './device-card/device-card.component';
import { MeasurmentCardComponent } from './measurment-card/measurment-card.component';
import {MatDividerModule} from '@angular/material/divider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { ScheduleComponent } from './schedule/schedule.component';


@NgModule({
  declarations: [
    AppComponent,
    DeviceCardComponent,
    MeasurmentCardComponent,
    ScheduleComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    MatCardModule,
    MatDividerModule,
    BrowserAnimationsModule,
    MatSlideToggleModule,

    MatDialogModule,
    MatButtonModule,
    MatProgressSpinnerModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
