import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { Measurment, Toggles } from '../interfaces/measurment-configuration';
import { DataSourceService } from '../services/data-source.service';

@Component({
  selector: 'app-measurment-card',
  templateUrl: './measurment-card.component.html',
  styleUrls: ['./measurment-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeasurmentCardComponent implements OnInit {

  measurmentCard$: Observable<Measurment>;
  togglesStatus: Toggles[];
  // color: 'grey';
  // disabled = false;

  // checked = true;

  constructor(private dataSourceService: DataSourceService) { }

  ngOnInit(): void {
    this.measurmentCard$ = this.dataSourceService.createMeasurmentCardStream();
    this.measurmentCard$.pipe(
      tap((data:Measurment) => this.togglesStatus = data.toggles)).subscribe();
  }

  changeToggleStatus(name:string):void{
    this.togglesStatus.map(item => item.name === name ? item.status = !item.status : item)
  }

}
