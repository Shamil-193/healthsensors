import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasurmentCardComponent } from './measurment-card.component';

describe('MeasurmentCardComponent', () => {
  let component: MeasurmentCardComponent;
  let fixture: ComponentFixture<MeasurmentCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeasurmentCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MeasurmentCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
