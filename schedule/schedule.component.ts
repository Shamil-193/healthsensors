import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { ScheduleCard, SelectedSchedule } from "../interfaces/schedule";
import { DataSourceService } from "../services/data-source.service";

@Component({
  selector: "app-schedule",
  templateUrl: "./schedule.component.html",
  styleUrls: ["./schedule.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScheduleComponent implements OnInit {

  scheduleCard$: Observable<ScheduleCard>;
  timeArrayAM = ["3:00", "3:30", "4:00", "4:30", "5:00", "5:30", "6:00", "6:30", "7:30", "8:00", "8:30", "9:00", "9:30", "10:00", "10:30", "11:00", "11:30"];
  timeArrayPM = ["12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00"];
  todayDate: Date;
  previousDate: Date;
  nextDate: Date;
  currentDate: Date;
  selectedSchedule: SelectedSchedule = {};

  constructor(private dataSourceService: DataSourceService) {
  }

  ngOnInit(): void {
    this.init();
  }

  private init() {
    this.scheduleCard$ = this.dataSourceService.createScheduleCardStream();
    this.todayDate = new Date();
    this.currentDate = new Date();
    this.previousDate = new Date((new Date()).setDate((new Date()).getDate() - 1));
    this.nextDate = new Date((new Date()).setDate((new Date()).getDate() + 1));
  }

  changeCurrentDate(day: number): void {
    this.currentDate = new Date(this.currentDate.setDate(this.currentDate.getDate() + day));
    this.previousDate = new Date(this.previousDate.setDate(this.previousDate.getDate() + day));
    this.nextDate = new Date(this.nextDate.setDate(this.nextDate.getDate() + day));
  }


  setTime(time: string, dayPart: string): void {
    const newDate: string = this.currentDate.toString();
    if(!(newDate in this.selectedSchedule)){
      this.selectedSchedule = {...this.selectedSchedule,[newDate]:{AM:[], PM:[]}};
    }
    dayPart === "AM" ? this.selectedSchedule[newDate]["AM"].push(time) : this.selectedSchedule[newDate]["PM"].push(time);
  }

  isSelected(time:string, dayPart: string):boolean{
    const newDate: string = this.currentDate.toString();
    if(!(newDate in this.selectedSchedule)){
      return false
    }
    return dayPart === "AM" ? this.selectedSchedule[newDate]["AM"].includes(time) : this.selectedSchedule[newDate]["PM"].includes(time)
}

}
