import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Device } from "../interfaces/device";
import { Measurment } from "../interfaces/measurment-configuration";
import { ScheduleCard } from "../interfaces/schedule";

@Injectable({
  providedIn: "root"
})
export class DataSourceService {

  exampleDeviceCard: Device = {
    image: "https://image.winudf.com/v2/image1/Y29tLmludGVyYmlyZC5kaXN0cmlidXRvcl9hbGxkYXRhc2hlZXRfaWNvbl8xNTQ2MDI5MDE3XzA4OA/icon.png?w=340&fakeurl=1",
    title: "Atmel",
    subtitle: "Atmega2560"
  }

  exampleMeasurment: Measurment = {
    title: "Heart Rate",
    image: "https://i7.pngflow.com/pngimage/6/487/png-computer-icons-pulse-heart-rate-symbol-symbol-love-miscellaneous-text-heart-clipart-thumb.png",
    toggles: [{ name: "Share", status: true }, { name: "Alarm", status: false }],
    options: [{ name: "Low", value: "55 bpm" }, { name: "High", value: "110 bpm" }]
  }

  exampleSchedule: ScheduleCard = {
    runTime: 15
  }

  constructor() { }

  createDeviceCardStream(): Observable<Device> {
    return new Observable<Device>(
      observer => {
        try {
          observer.next(this.exampleDeviceCard)
        } catch (error) {
          observer.error(error)
        }
      }
    )
  }

  createMeasurmentCardStream(): Observable<Measurment> {
    return new Observable<Measurment>(
      observer => {
        try {
          observer.next(this.exampleMeasurment)
        } catch (error) {
          observer.error(error)
        }
      }
    )
  }

  createScheduleCardStream(): Observable<ScheduleCard> {
    return new Observable<ScheduleCard>(
      observer => {
        try {
          observer.next(this.exampleSchedule)
        } catch (error) {
          observer.error(error)
        }
      }
    )
  }

}
